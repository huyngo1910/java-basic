package tutorial;

import java.io.*;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

public class sample {

    public static void main(String args[]) {

        String EMAIL_PATTERN =
                "^[a-zA-Z][\\w-]+@([\\w]+\\.[\\w]+|[\\w]+\\.[\\w]{2,}\\.[\\w]{2,})$";
//
//		System.out.println("Bai tap 3: Viet chuong trinh in ra so lan ky tu a xuat hien trong mot chuoi.\n");
//			String string;
//		    char character = 'a';
//		    int count = 0;
//		    Scanner scanner = new Scanner(System.in);
//
//		    System.out.print("Nhap vao chuoi bat ki: ");
//		    string = scanner.nextLine();
//
//		    for (int i = 0; i < string.length(); i++) {
//		        if (string.charAt(i) == character) {
//		            count++;
//		        }
//		    }
//
//		    System.out.print("So lan xuat hien ky tu " + character +
//		        " trong chuoi " + string + " la: " + count + "\n");
//		scanner.close();

//		System.out.println("Bai tap 4: Viet phuong thuc kiem tra chuoi nhap vao co dung dinh dang email khong?");
//
//		Scanner checking = new Scanner(System.in);
//		String email;
//
//		System.out.print("Nhap vao email cua ban de kiem tra: ");
//		email = checking.nextLine();
//        System.out.println("Dinh dang cua email " +  email + ": " + Pattern.matches(EMAIL_PATTERN, email));
//        checking.close();
//
//


        System.out.println("Bai tap 5: Viet chuong trinh doc 1 file text (Nhap tu ban phim duong dan file),\n"
                + "tim cac dong co chu \"VIS training\" va ghi cac dong tim duoc ra 1 file khac.");

//		try {
//			BufferedWriter bw = new BufferedWriter(
//					new FileWriter("C:\\Users\\caunh\\work\\draft\\output.txt"));
//			bw.write("something\n");
//			bw.write("VIS TRAINING\n");
//			bw.write("VIS training\n");
//			bw.write("blablo\n");
//			bw.write("internship at VIS Soft");
//			bw.write("VIS training\n");
//			bw.write("VIS TrAiNinG\n");
//			bw.write("heheVIS traininghehe\n");
//			bw.write("something\n");
//			bw.close();
//		}
//		catch(Exception ex){
//			return;
//		}
        Scanner sc = new Scanner(System.in);
        System.out.print("Vui long nhap duong dan file: ");
        String path = sc.nextLine();

        // C:\\Users\\caunh\\work\\draft\\output.txt
        File f = new File(path);
        try {
            FileInputStream fis = new FileInputStream(f);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            FileOutputStream fos = new FileOutputStream(new File("output.txt"));
            DataOutputStream os = new DataOutputStream(fos);

            ArrayList<String> arrS = new ArrayList();
            String s = br.readLine();
            while (s != null) {
                System.out.println(s);
                arrS.add(s);
                s = br.readLine();
            }

            String match = "vis training";
            for (int i = 0; i < arrS.size(); i++) {
                if (arrS.get(i).toLowerCase().indexOf(match) != -1) {
                    os.writeChars(arrS.get(i) + "\n");
                }
            }
            fis.close();
            br.close();
            fos.close();
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
            }
        }