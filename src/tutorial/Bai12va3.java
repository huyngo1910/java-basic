package tutorial;

import java.util.Scanner;

public class Bai12va3{
	public static void main(String args[])
	{
		System.out.println("Hello World \n");
		System.out.println("Bai tap 1: Tinh tong 10 so chan dau tien \n");
		
		int i1 = 0, n1 = 0, sumEvenNum = 0;  
		while (i1 < 10)
		{
            System.out.print(n1 + "\t");
            i1+=1;
            sumEvenNum += n1;
            n1+=2;
		}
        System.out.println();
		System.out.print("Tong 10 so chan dau tien: " + sumEvenNum);
		
		System.out.println("\n\nBai tap 2: Tinh tong n so nguyen to dau tien \n");
		
	    Scanner sc=new Scanner(System.in);
	    int n,sum=0,i=1,j;
	    System.out.print("Nhap so so nguyen to ban muon liet ke: ");
	    n=sc.nextInt();
	    int t=n;
	      System.out.print(t+ " so nguyen to dau tien lan luot la: ");
	    while(n!=0)
	    {
	      int count=0;
	      for(j=1;j<=i;j++)
	      {
	        if(i%j==0)
	        {
	          count++;
	        }
	      }
	      if(count==2)
	      {
	    	  System.out.print(" " + i);
	        sum+=i;
	        n--;
	      }
	      i++;
	    }
	    System.out.println("\nTong cua "+t+" so nguyen to dau tien la: "+sum);
	    sc.close();
	}
}
