package tutorial;

public class phoneFormat {
    private String phoneNumber;
    private String network;
    private String totalAmount;

    public phoneFormat() {}

    public phoneFormat(String phoneNumber, String network, String totalAmount) {
        this.phoneNumber = phoneNumber;
        this.network = network;
        this.totalAmount = totalAmount;
    }

    public String getNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.network = network;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return "So: " + phoneNumber + ", Nha mang: " + network + ", So tien: " + totalAmount + "\n";
    }
}
